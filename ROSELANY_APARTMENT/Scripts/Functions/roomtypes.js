﻿
$(document).ready(function () {
    $('#RoomTypeModal').on('show.bs.modal', function () {
        $(document).ajaxStart(function () {
            $('#loadingGif').addClass('show');
        }).ajaxStop(function () {
            $('#loadingGif').removeClass('show');
        });
        GetRoomType();
        DisableControlRoomType();
        $('#btnAddRoomType').show();
        $('#btnSaveRoomType').hide();
        $('#btnUpdateRoomType').hide();
    });
});

toastr.optionsOverride = 'positionclass = "toast-bottom-right"';
toastr.options.positionClass = 'toast-bottom-right';
toastr.options.progressBar = true;

var tableRoomType = [];
function GetRoomType() {
    tableRoomType = $('#tableRoomType').dataTable({
        ajax: {
            url: "/api/RoomTypes",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id"
                },
                {
                    data: "roomtypename"
                },
                {
                    data: "roomtypenamekh"
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='OnEditRoomType (" + data + ")' class='btn btn-warning btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span></button>" +
                            "<button OnClick='OnDeleteRoomType (" + data + ")' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>";
                    }
                }
            ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false

    });
}

function OnEditRoomType(id) {
    $('#btnAddRoomType').show();
    $('#btnSaveRoomType').hide();
    $('#btnUpdateRoomType').show();
    EnableControlRoomType();
    $.ajax({
        url: "/api/RoomTypes/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#RoomTypeid').val(result.id);
            $("#roomtypename").val(result.roomtypename);
            $("#roomtypenamekh").val(result.roomtypenamekh);
            $("#note").val(result.note);
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });

}

function OnDeleteRoomType(id) {
    bootbox.confirm({
        title: "",
        message: "<h3>Are you sure want to delete record " + id + " ?</h3>",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success btn-sm'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger btn-sm'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/RoomTypes/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#tableRoomType').DataTable().ajax.reload();
                        toastr.success("RoomType Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("RoomType Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}

function DisableControlRoomType() {
    document.getElementById('roomtypename').disabled = true;
    document.getElementById('roomtypenamekh').disabled = true;
    document.getElementById('note').disabled = true;
}

function EnableControlRoomType() {
    document.getElementById('roomtypename').disabled = false;
    document.getElementById('roomtypenamekh').disabled = false;
    document.getElementById('note').disabled = false;
}

function ClearControlRoomType() {
    $('#roomtypename').val('');
    $('#roomtypenamekh').val('');
    $('#note').val('');
}