﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        $('#loadingGif').addClass('show');
    }).ajaxStop(function () {
        $('#loadingGif').removeClass('show');
    });
    $('#PositionModal').on('show.bs.modal', function () {
        GetPosition();
        document.getElementById('positionname').disabled = true;
        document.getElementById('positionnamekh').disabled = true;

        $('#btnAddPosition').show();
        $('#btnSavePosition').hide();
        $('#btnUpdatePosition').hide();
    });
})

toastr.optionsOverride = 'positionclass = "toast-bottom-right"';
toastr.options.positionClass = 'toast-bottom-right';
toastr.options.progressBar = true;

var tablePosition = [];
function GetPosition() {
    tablePosition = $('#positionTable').DataTable({
        ajax: {
            url: "/api/position",
            dataSrc: ""
        },
        columns: [
            {
                data: "id"
            },
            {
                data: "positionname"
            },
            {
                data: "positionnamekh"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='PositionEdit(" + data + ")' class='btn btn-warning btn-xs' style='margin-right:5px;'><span class='glyphicon glyphicon-edit'></span></button>"
                        + "<button onclick='PositionDelete(" + data + ")' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>";
                }
            }
        ],
        destroy: true,
        "order": [0, "desc"],
        "info": false
    });
}



function PositionEdit(id) {
    $('#btnAddPosition').show();
    $('#btnSavePosition').hide();
    $('#btnUpdatePosition').show();

    $.ajax({
        url: "/api/position/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#positionid').val(result.id);
            $('#positionname').val(result.positionname);
            $('#positionnamekh').val(result.positionnamekh);
            $('#status').val(result.status);

            document.getElementById('positionname').disabled = false;
            document.getElementById('positionnamekh').disabled = false;
            $('#positionname').focus();
        },
        error: function (errormessage) {
            toastr.error("This Position is already exists in Database", "Service Response");
        }
    });

}


function PositionDelete(id) {
    alert(id);
    bootbox.confirm({
        title: "",
        message: "<h3>Are you sure want to delete record " + id + " ?</h3>",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success btn-sm'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger btn-sm'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/position/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        tablePosition.ajax.reload();
                        toastr.success("Position has been Deleted successfully!", "Service Response");
                        document.getElementById('positionname').disabled = true;
                        document.getElementById('positionnamekh').disabled = true;
                        document.getElementById('btnPosition').innerText = "Add New";
                    },
                    error: function (errormessage) {
                        toastr.error("This Position cannot delete it already use in Database", "Service Response");
                    }
                });
            }
        }
    });
}

