﻿$(document).ready(function () {
    GetStaff();
})

toastr.optionsOverride = 'positionclass = "toast-bottom-right"';
toastr.options.positionClass = 'toast-bottom-right';
toastr.options.progressBar = true;

var tableStaff = [];
function GetStaff() {
    tableStaff = $('#tableStaff').DataTable({
        ajax: {
            url: "/api/staffs",
            dataSrc: ""
        },
        columns: [
            {
                data: "photo",
                render: function (data) {
                    return "<img  src='Images/"+data+"' style='width: 60px; height: 60px' />";
                }
            },
            {
                data: "name"
            },
            {
                data: "namekh"
            },
            {
                data: "sex"
            },
            {
                data: "phone"
            },
            {
                data: "dob",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "address"
            },
            {
                data: "email"
            },
            {
                data: "identityno"
            },
            {
                data: "position.positionnamekh"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='EditStaff(" + data + ")' class='btn btn-warning btn-xs' style='margin-right:5px;'><span class='glyphicon glyphicon-edit'></span></button>"
                        + "<button onclick='DeleteStaff(" + data + ")' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>";
                }
            }
        ],
        destroy: true,
        "order": [0, "desc"],
        "info": false
    });
}


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#photo').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function NewStaff() {
    $("#StaffModal").modal('show');
    $('#btnUpdateStaff').hide() == true;
}

function EditStaff(id) {
    $("#StaffModal").modal('show');
    $('#btnSaveStaff').hide() == true;
    $.ajax({
        url: "/api/staffs/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#staffid').val(result.id);
            $('#staffname').val(result.name);
            $('#staffnamekh').val(result.namekh);
            $('#position').val(result.positionid).change();
            $('#sex').val(result.sex).change();
            $('#phone').val(result.phone);
            var dr = moment(result.dob).format("YYYY-MM-DD");
            $("#dob").val(dr);
            $('#address').val(result.address);
            $('#email').val(result.email);
            $('#identityno').val(result.identityno);
            $('#file_old').val(result.photo);
            if (result.photo == "") {
                $('#photo').attr('src', '../Images/no_image.png');
            } else {
                $('#photo').attr('src', '../Images/' + result.photo);
            }

        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });
}

function DeleteStaff(id) {
    bootbox.confirm({
        title: "",
        message: "<h3>Are you sure want to delete record "+id+" ?</h3>",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success btn-sm'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger btn-sm'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/staffs/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        toastr.success("Record delete successfully!", "Service Response");
                        window.location.reload(true);
                    },
                    error: function (errormessage) {
                        toastr.error("Record delte faild!", "Service Response");
                    }
                });
            }
        }
    });
}