﻿$(document).ready(function () {
    $('#RoomItemModal').on('show.bs.modal', function () {
        $(document).ajaxStart(function () {
            $('#loadingGif').addClass('show');
        }).ajaxStop(function () {
            $('#loadingGif').removeClass('show');
        });

        GetRoomItem();
        DisableControlRoomItem();
        $('#btnAddNewRoomItem').show();
        $('#btnSaveRoomItem').hide();
        $('#btnUpdateRoomItem').hide();
    });
});
toastr.optionsOverride = 'positionclass = "toast-bottom-right"';
toastr.options.positionClass = 'toast-bottom-right';
toastr.options.progressBar = true;

var RoomItemTable = [];
function GetRoomItem() {
    RoomItemTable = $('#tblRoomItem').dataTable({
        ajax: {
            url: "/api/roomdetails",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id"
                },
                {
                    data: "room_no"
                },
                {
                    data: "itemname"
                },
                {
                    data: "price"
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='OnRoomItemEdit (" + data + ")' class='btn btn-warning btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span></button>" +
                               "<button OnClick='OnDeletRoomItem (" + data + ")' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>";
                    }
                }
            ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false

    });
}

function AddNewRoomItem() {
    $('#btnAddNewRoomItem').hide();
    $('#btnSaveRoomItem').show();
    $('#btnUpdateRoomItem').hide();
    EnableControlRoomItem();
    ClearControlRoomItem();
}


function OnRoomItemEdit(id) {
    $('#btnAddNewRoomItem').show();
    $('#btnSaveRoomItem').hide();
    $('#btnUpdateRoomItem').show();
    EnableControlRoomItem();
    $.ajax({
        url: "/api/roomdetails/"+id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#roomitemid').val(result.id);
            $("#iroomid").val(result.roomid);
            $("#itemid").val(result.itemid);
            $("#iprice").val(result.price);
            //alert(result.room_no);
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });

}

function OnDeletRoomItem(id) {
    bootbox.confirm({
        title: "",
        message: "<h3>Are you sure want to delete record " + id + " ?</h3>",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success btn-sm'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger btn-sm'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/roomdetails/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        toastr.success("Record delete successfully!", "Service Response");
                        $('#tblRoomItem').DataTable().ajax.reload();
                    },
                    error: function (errormessage) {
                        toastr.error("Record delte faild!", "Service Response");
                    }
                });
            }
        }
    });
}

function DisableControlRoomItem() {
    document.getElementById('iroomid').disabled = true;
    document.getElementById('itemid').disabled = true;
    document.getElementById('iprice').disabled = true;
}

function EnableControlRoomItem() {
    document.getElementById('iroomid').disabled = false;
    document.getElementById('itemid').disabled = false;
    document.getElementById('iprice').disabled = false;
}

function ClearControlRoomItem() {
    $('#iroomid').val('');
    $('#itemid').val('');
    $('#iprice').val('');
}
