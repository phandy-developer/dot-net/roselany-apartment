﻿$(document).ready(function () {
    $('#RoomModal').on('show.bs.modal', function () {
        $(document).ajaxStart(function () {
            $('#loadingGif').addClass('show');
        }).ajaxStop(function () {
            $('#loadingGif').removeClass('show');
        });
        GetRoom();
        DisableControlRoom();
        $('#btnAddRoom').show();
        $('#btnSaveRoom').hide();
        $('#btnUpdateRoom').hide();
    });
});

toastr.optionsOverride = 'positionclass = "toast-bottom-right"';
toastr.options.positionClass = 'toast-bottom-right';
toastr.options.progressBar = true;
var tableRoom = [];
function GetRoom() {
    tableRoom = $('#tableRoom').dataTable({
        ajax: {
            url: "/api/Rooms",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id"
                },
                {
                    data: "room_no"
                },
                {
                    data: "price"
                },
                {
                    data: "roomtypename"
                },
                {
                    data: "floorno"
                },               
                {
                    data: "status",
                    render: function (data) {
                        if (data == "FREE") {
                            return "<span class='label label-primary'>" + data + "</span>";
                        } else if (data == "BOOK") {
                            return "<span class='label label-success'>" + data + "</span>";
                        } else if (data == "CHECK-IN") {
                            return "<span class='label label-warning'>" + data + "</span>";
                        } else if (data == "BLOCK") {
                            return "<span class='label label-danger'>" + data + "</span>";
                        }
                    }
                    
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='OnEditRoom (" + data + ")' class='btn btn-warning btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span></button>"
                            + "<button OnClick='DeleteRoom (" + data + ")' class='btn btn-danger btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-trash'></span></button>"                           
                            ;
                    }
                }
            ],
        destroy: true,
        "info": false

    });
}

function OnEditRoom(id) {
    EnableControlRoom();
    $('#btnAddRoom').show();
    $('#btnSaveRoom').hide();
    $('#btnUpdateRoom').show();
    $.ajax({
        url: "/api/Rooms/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#Roomid').val(result.id);

            $("#roomno").val(result.room_no);
            $("#roomtypeid").val(result.roomtypeid);
            $("#floorid").val(result.floorid);
            $("#servicecharge").val(result.servicecharge);
            $("#price").val(result.price);
            $("#roomkey").val(result.roomkey);
            $("#roomstatus").val(result.status);
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });
}

function DeleteRoom(id) {
    bootbox.confirm({
        title: "",
        message: "<h3>Are you sure want to delete record " + id + " ?</h3>",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success btn-sm'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger btn-sm'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/Rooms/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#tableRoom').DataTable().ajax.reload();
                        toastr.success("Room Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Room Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}

function DisableControlRoom() {
    document.getElementById('roomno').disabled = true;
    document.getElementById('roomtypeid').disabled = true;
    document.getElementById('floorid').disabled = true;
    document.getElementById('price').disabled = true;
    document.getElementById('servicecharge').disabled = true;
    document.getElementById('roomkey').disabled = true;
    document.getElementById('roomstatus').disabled = true;
}

function EnableControlRoom() {
    document.getElementById('roomno').disabled = false;
    document.getElementById('roomtypeid').disabled = false;
    document.getElementById('floorid').disabled = false;
    document.getElementById('price').disabled = false;
    document.getElementById('servicecharge').disabled = false;
    document.getElementById('roomkey').disabled = false;
    document.getElementById('roomstatus').disabled = false;

}

function ClearControlRoom() {
    $('#roomno').val('');
    $('#price').val('0');
    $('#servicecharge').val('0');
    $('#roomkey').val('');

}

function ValidationFormRoom() {
    var isValid = true;
    if ($('#roomno').val().trim() === "") {
        $('#roomno').css('border-color', 'red');
        $('#roomno').focus();
        isValid = false;
    }
    else {
        $('#roomno').css('border-color', '#cccccc');
        if ($('#price').val().trim() === "") {
            $('#price').css('border-color', 'red');
            $('#price').focus();
            isValid = false;
        }
        else {
            $('#price').css('border-color', '#cccccc');
            if ($('#roomkey').val().trim() === "") {
                $('#roomkey').css('border-color', 'red');
                $('#roomkey').focus();
                isValid = false;
            }
            else {
                $('#roomkey').css('border-color', '#cccccc');
            }
        }
           
    }
    return isValid;
}


