﻿
$(document).ready(function () {
    $('#BuildingModal').on('show.bs.modal', function () {
        $(document).ajaxStart(function () {
            $('#loadingGif').addClass('show');
        }).ajaxStop(function () {
            $('#loadingGif').removeClass('show');
        });
        GetBuilding();
        document.getElementById('buildingname').readOnly = true;
        document.getElementById('buildingnamekh').readOnly = true;

        $('#btnAddBuilding').show();
        $('#btnSaveBuilding').hide();
        $('#btnUpdateBuilding').hide();
    });
});

var tableBuilding = [];
function GetBuilding() {
    tableBuilding = $('#tableBuilding').dataTable({
        ajax: {
            url: "/api/buildings",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id"
                },
                {
                    data: "buildingname"
                },
                {
                    data: "buildingnamekh"
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='OnEditBuilding (" + data + ")' class='btn btn-warning btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span></button>" +
                               "<button OnClick='OnDeleteBuilding (" + data + ")' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>";
                    }
                }
            ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false

    });
}

function OnEditBuilding(id) {
    $('#btnAddBuilding').show();
    $('#btnSaveBuilding').hide();
    $('#btnUpdateBuilding').show();

    document.getElementById('buildingname').readOnly = false;
    document.getElementById('buildingnamekh').readOnly = false;
    $.ajax({
        url: "/api/buildings/"+id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            $('#buildingid').val(result.id);
            $('#buildingname').val(result.buildingname);
            $('#buildingnamekh').val(result.buildingnamekh);
        },
        error: function (errormesage) {
            toastr.error("Bad request!" + errormesage, "Server Respond");
        }
    });
}

function OnDeleteBuilding(id) {
    bootbox.confirm({
        title: "",
        message: "<h3>Are you sure want to delete record " + id + " ?</h3>",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success btn-sm'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger btn-sm'
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/buildings/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        tableBuilding.DataTable().ajax.reload();
                        toastr.success("Delete record successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Delete record faild!", "Service Response");
                    }
                });
            }
        }
    });

}

function validateForm() {
    var isValid = true;
    if ($('#buildingname').val().trim() == "") {
        $('#buildingname').css('border-color', 'red');
        $('#buildingname').focus();
        isValid = false;
    } else {
        $('#buildingname').css('border-color', '#cccccc');
        if ($('#buildingnamekh').val().trim() == "") {
            $('#buildingnamekh').css('border-color', 'red');
            $('#buildingnamekh').focus();
            isValid = false;
        }else{
            $('#buildingnamekh').css('border-color', '#cccccc');
        }
    }
    return isValid;
}